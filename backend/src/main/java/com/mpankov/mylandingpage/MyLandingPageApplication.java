package com.mpankov.mylandingpage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyLandingPageApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyLandingPageApplication.class, args);
	}

}
